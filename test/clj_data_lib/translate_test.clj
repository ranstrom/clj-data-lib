(ns clj-data-lib.translate-test
  (:require [clj-data-lib.translate :as trn]
            [clojure.test :refer [deftest is testing]]))

(deftest test-bigdecimal->int
  (testing "bigdecimal->int successfully"
    (is (= (trn/bigdecimal->plainString (BigDecimal. "0000.0000001000"))
           "0.0000001"))))