(disable-warning
 {:linter :suspicious-expression
  :for-macro 'clojure.core/or
  :if-inside-macroexpansion-of #{'clj-data-lib.data/or+}
  :reason "Doesn't function properly for some reason..."})