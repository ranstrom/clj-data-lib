(ns clj-data-lib.time
  (:require [clj-data-lib.jt2 :as jt]
            [clojure.instant :as instant]
            [clojure.string :as string])
  (:import (java.time ZonedDateTime ZoneId Instant LocalDateTime LocalDate ZoneOffset DayOfWeek
                      LocalTime Month)
           (java.time.format TextStyle)
           (java.time.temporal TemporalAdjusters WeekFields)
           (java.sql Date Timestamp)
           (java.util Locale)
           (org.threeten.extra YearWeek Quarter)))

(defn- try->bool [f]
  (try
    (f)
    true
    (catch Exception _
      false)))

(defn- try->type [f]
  (try
    (f)
    (catch Exception _
      nil)))

(def standard-date-format "yyyy-MM-dd HH:mm:ss.SSS")

(def standard-file-date-format "yyyy-MM-dd'T'HHmmss")

(defn time-offset-fn [offset-type]
  (get jt/units-chrono offset-type))

(defn offset-date-time [ldt ^Integer offset-amount offset-type]
  (.plus ldt offset-amount (time-offset-fn offset-type)))

(defn get-instant
  ([]
   (get-instant 0))
  ([offset-minutes]
   (get-instant offset-minutes :minutes))
  ([offset-amount offset-type]
   (offset-date-time (jt/instant) offset-amount offset-type)))

(defn instant-offset [offset-type offset-amount]
  (get-instant offset-amount offset-type))

(defn- instant->ZonedDateTime [i] (ZonedDateTime/ofInstant i (ZoneId/of "UTC")))

(defn- instant-as-ZonedDateTime
  ([]
   (instant-as-ZonedDateTime 0))
  ([offset-minutes]
   (instant->ZonedDateTime (get-instant offset-minutes))))

(defn instant-as-str
  ([]
   (instant-as-str 0))
  ([offset-minutes]
   (jt/jt-format standard-date-format (instant-as-ZonedDateTime offset-minutes))))

(defn instant-as-file-friendly-str []
  (jt/jt-format standard-file-date-format (instant-as-ZonedDateTime)))

(defn- is-type? [a t] (= t (type a)))

(defn java-sql-date? [a] (is-type? a Date))
(defn java-sql-timestamp? [a] (is-type? a Timestamp))
(defn java-util-date? [a] (is-type? a java.util.Date))
(defn java-time-instant? [a] (is-type? a Instant))
(defn java-time-date-time? [a] (is-type? a LocalDateTime))
(defn java-time-date? [a] (is-type? a LocalDate))
(defn java-time? [a] (is-type? a LocalTime))

(defn any-date-type? [a]
  (some
   #(% a)
   [java-sql-date?
    java-util-date?
    java-time-date?]))

; Supported date-time types
(defn any-date-time-type? [a]
  (some
   #(% a)
   [java-sql-date?
    java-sql-timestamp?
    java-util-date?
    java-time-instant?
    java-time-date-time?
    java-time-date?]))

(defn- local-date->local-date-time ^LocalDateTime
  ([^LocalDate ld]
   (.atStartOfDay ld))
  ([^LocalDate ld h]
   (local-date->local-date-time ld h 0))
  ([^LocalDate ld h mm]
   (local-date->local-date-time ld h mm 0))
  ([^LocalDate ld h mm ss]
   (local-date->local-date-time ld h mm ss 0))
  ([^LocalDate ld h mm ss n]
   (.atTime ld (int h) (int mm) (int ss) (int n))))

(defn- str-dt-replace [a] (string/replace-first a #" " "T"))

(defn str-is-date-time? [a]
  (try->bool #(jt/local-date-time (str-dt-replace a))))

(defn str-is-date? [a]
  (try->bool #(jt/local-date a)))

(defn str->inst [a] (instant/read-instant-date (str-dt-replace a)))

(defn str-is-inst? [a]
  (try->bool #(str->inst a)))

(defn- time-formats []
  (for [hh ["H" "HH"]
        mm [":m" ":mm"]
        ss ["" ":ss"]
        sss ["" ".SSS"]
        a [nil " a"]]
    (string/join [(if a (string/lower-case hh) hh) mm ss sss a])))

(defn str->local-time [a]
  (or (try->type #(LocalTime/parse a))
      (some (fn [v] (try->type #(LocalTime/parse a (jt/formatter v)))) (time-formats))
      (throw (Exception. (str "Invalid time format: " a)))))

(defn str-is-time? [a]
  (try->bool #(str->local-time a)))

(defn str-time-parseable? [a]
  (some #(% a) [str-is-date-time? str-is-date? str-is-inst?]))

(def month-keywords
  #{:january :february :march :april :may :june :july :august :september :october :november :decembe})

(def day-of-week-keywords
  #{:monday :tuesday :wednesday :thursday :friday :saturday :sunday})

(defn- keyword-match [v ks]
  (when (or (string? v) (keyword? v))
    (let [vmatch (-> v name string/lower-case keyword)]
      (if (= (count (name v)) 3)
        (some #(when (= vmatch (-> % name (subs 0 3) keyword)) %) ks)
        (some #{vmatch} ks)))))

(defn str-is-month? [a]
  (keyword-match a month-keywords))

(defn str->month [a]
  (-> a str-is-month? name string/upper-case (Month/valueOf)))

(defn str-is-day-of-week? [a]
  (keyword-match a day-of-week-keywords))

(defn str->day-of-week [a]
  (-> a str-is-day-of-week? name string/upper-case (DayOfWeek/valueOf)))

(defn java-util-date->local-date-time [a]
  (-> a (.toInstant) (instant->ZonedDateTime) (.toLocalDateTime)))

(defn type->local-date-time [a]
  (try
    (cond
      (java-time-date-time? a) a
      (java-time-instant? a) (LocalDateTime/ofInstant a ZoneOffset/UTC)
      (java-time-date? a) (local-date->local-date-time a)
      (java-sql-date? a) (local-date->local-date-time (.toLocalDate ^Date a))
      (java-sql-timestamp? a) (.toLocalDateTime ^Timestamp a)
      (or (inst? a) (java-util-date? a)) (java-util-date->local-date-time a)
      (and (string? a) (str-is-date-time? a)) (jt/local-date-time (str-dt-replace a))
      (and (string? a) (str-is-date? a)) (local-date->local-date-time (jt/local-date a))
      (and (string? a) (str-is-inst? a)) (java-util-date->local-date-time (str->inst a))
      :else (jt/local-date-time a))
    (catch Exception err
      (throw (Exception. (str "Error for type->local-date-time using input: " a "\n" err))))))

(defn type->local-date [a]
  (-> a type->local-date-time (.toLocalDate)))

(defn type->instant [a]
  (-> a type->local-date-time (.atZone (ZoneId/of "UTC")) (.toInstant)))

(defn type->sql-date [a]
  (-> a type->local-date jt/to-sql-date))

(defn type->sql-date-time [a]
  (-> a type->local-date-time jt/to-sql-timestamp))

(defn type->month [a]
  (cond
    (is-type? a Month) a
    (str-is-month? a) (str->month a)
    :else (-> a type->local-date-time (.getMonth))))

(defn type->day-of-week [a]
  (cond
    (is-type? a DayOfWeek) a
    (str-is-day-of-week? a) (str->day-of-week a)
    :else (-> a type->local-date-time (.getDayOfWeek))))

(defn type->local-time [a]
  (cond
    (java-time? a) a
    (str-is-time? a) (str->local-time a)
    :else (-> a type->local-date-time (.toLocalTime))))

(defn type-> [v]
  (cond
    (java-sql-date? v) :sql-date
    (java-sql-timestamp? v) :sql-date-time
    ;; (java-util-date? v) :util-date
    (java-time-instant? v) :instant
    (java-time-date-time? v) :local-date-time
    (java-time-date? v) :local-date
    :else (throw (Exception. (str "Unsupported date/time type: " (type v) " for " v)))))

(defn ->type [v date-type]
  ((case date-type
     :sql-date type->local-date
     :sql-date-time type->sql-date-time
     :instant type->sql-date-time
     :local-date-time type->local-date-time
     :local-date type->local-date
     (throw (Exception. (str "Unsupported input date-type: " date-type " for " v)))) v))

(defn type->type-fn
  "Applies a date-time function to the input value and ensures the return value is of same type."
  [v type-fn]
  (->type (type-fn (type->local-date-time v)) (type-> v)))

(defn type->str
  ([a]
   (type->str a standard-date-format))
  ([a date-format]
   (->> a type->local-date-time (jt/jt-format date-format))))

(defn date-time-str->local-date-time [a date-format]
  (jt/local-date-time a date-format))

(defn date-str->local-date [a date-format]
  (jt/local-date a date-format))

(defn date-str->str
  ([a in-date-format]
   (date-str->str a in-date-format standard-date-format))
  ([a in-date-format out-date-format]
   (try
     (type->str (or (try->type #(date-time-str->local-date-time a in-date-format))
                    (try->type #(date-str->local-date a in-date-format)))
                out-date-format)
     (catch Exception e
       (throw (Exception. (str "Unable to parse " a " with format " in-date-format ": " e)))))))

(defn instant->epoch [^Instant v]
  (.toEpochMilli v))

(defn type->epoch [v]
  (when v (-> v type->instant instant->epoch)))

(defn- args->compare-value [& [args]]
  (cond
    (every? java-time? args)
    (map #(.toNanoOfDay %) args)
    (or (every? #(is-type? % Month) args)
        (every? #(is-type? % DayOfWeek) args))
    (map #(.getValue %) args)
    :else
    (map type->epoch args)))

(defn- apply-compare [f & [args]]
  (apply f (args->compare-value args)))

(defn time= [& args] (apply-compare = args))

(defn time> [& args] (apply-compare > args))

(defn time< [& args] (apply-compare < args))

(defn time>= [& args] (apply-compare >= args))

(defn time<= [& args] (apply-compare <= args))

(defn- reduce-by-fn [f [& args]]
  (reduce
   (fn [v1 v2]
     (if (f v1 v2) v1 v2))
   (first args)
   (rest args)))

(defn time-min [& args]
  (reduce-by-fn time<= args))

(defn time-max [& args]
  (reduce-by-fn time>= args))

(defn ->last-day-of-month [v]
  (.with v (TemporalAdjusters/lastDayOfMonth)))

(defn ->first-day-of-month [v]
  (.with v (TemporalAdjusters/firstDayOfMonth)))

(defn ->last-day-of-year [v]
  (.with v (TemporalAdjusters/lastDayOfYear)))

(defn ->first-day-of-year [v]
  (.with v (TemporalAdjusters/firstDayOfYear)))

(defn date-ranges
  "Input start and optional end date, with frequency. Return vector of start/end dates."
  ([start-date frequency]
   (date-ranges start-date (jt/local-date) frequency))
  ([start-date end-date frequency]
   (let [time-offset (time-offset-fn frequency)
         days (time-offset-fn :days)
         start (type->local-date start-date)
         end (type->local-date end-date)]
     (when (time>= end start)
       (->> (jt/time-between start end frequency) inc range
            (map
             (fn [v]
               [(.plus ^LocalDate start v time-offset)
                (jt/jt-min ^LocalDate end
                           (.minus (.plus ^LocalDate start (inc v) time-offset) 1 days))])))))))

(defn date-time-ranges
  "Returns collection of date-time ranges as [[s1 e1] [s2 e2]]"
  ([start-date-time frequency]
   (date-time-ranges start-date-time (jt/local-date) frequency))
  ([start-date-time end-date-time frequency]
   (let [days (time-offset-fn :days)
         millis (time-offset-fn :millis)]
     (map
      (fn [[v0 v1]]
        [(type->local-date-time v0)
         (-> v1 type->local-date-time (.plus 1 days) (.minus 1 millis))])
      (date-ranges (type->local-date start-date-time)
                   (type->local-date end-date-time)
                   frequency)))))

(defn day-range
  "Returns collections of days between start- and end-date"
  [start-date end-date]
  (->> (jt/time-between start-date end-date :days)
       inc range
       (map #(.plus ^LocalDate start-date % (time-offset-fn :days)))))

(defn- first-day-of-frequency [a frequency]
  (case frequency
    :months (->first-day-of-month a)
    :years (->first-day-of-year a)
    a))

(defn- last-day-of-frequency [a frequency]
  (case frequency
    :months (->last-day-of-month a)
    :years (->last-day-of-year a)
    a))

(defn calendar-date-ranges
  ([start-date frequency]
   (calendar-date-ranges start-date (jt/local-date) frequency))
  ([start-date end-date frequency]
   (let [last-day-adj (last-day-of-frequency (offset-date-time end-date -1 frequency) frequency)]
     (date-ranges
      (first-day-of-frequency start-date frequency)
      (time-min end-date last-day-adj)
      frequency))))

(defn last-calendar-range
  ([frequency]
   (last-calendar-range (jt/local-date) frequency))
  ([^LocalDate start-date frequency]
   (let [end-date (offset-date-time start-date -1 frequency)
         adj-end-date (last-day-of-frequency end-date frequency)
         adj-start-date (first-day-of-frequency adj-end-date frequency)]
     [adj-start-date adj-end-date])))

(defn weekend? [^LocalDate v]
  (let [dow (.getDayOfWeek v)]
    (or (some #(.equals dow %) [DayOfWeek/SATURDAY DayOfWeek/SUNDAY]) false)))

(defn- date-in-coll? [v coll]
  (boolean (some #{v} (map type->local-date coll))))

(defn working-days
  ([start-date end-date]
   (working-days start-date end-date []))
  ([start-date end-date holidays]
   (->> (day-range start-date end-date)
        (map #(and (not (weekend? %))
                   (not (date-in-coll? % holidays))))
        (filter true?) count)))

(defn ->dayOfWeek [v]
  (case (keyword v)
    :monday DayOfWeek/MONDAY
    :tuesday DayOfWeek/TUESDAY
    :wednesday DayOfWeek/WEDNESDAY
    :thursday DayOfWeek/THURSDAY
    :friday DayOfWeek/FRIDAY
    :saturday DayOfWeek/SATURDAY
    :sunday DayOfWeek/SUNDAY))

(defn local-date->date-map
  ([^LocalDate v]
   (local-date->date-map v {}))
  ([^LocalDate v {:keys [holidays week-start min-week-days use-sql-date prior]}]
   (let [^WeekFields week-of (if week-start
                               (WeekFields/of (->dayOfWeek week-start) (or min-week-days 1))
                               (WeekFields/of (Locale/getDefault)))
         ->int    #(Integer/parseInt (type->str %1 %2))
         dow      (.getDayOfWeek v)
         qoy      (Quarter/from v)
         moy      (.getMonth v)
         weekday? (<= (.getValue dow) 5)
         holiday? (date-in-coll? v holidays)
         year (.getYear v)
         year_month (->int v "yyyyMM")]
     {:calendar_date        (if use-sql-date (type->sql-date v) v)
      :calendar_key         (->int v "yyyyMMdd")
      :day_abbr             (.getDisplayName dow TextStyle/SHORT (Locale/getDefault))
      :day_name             (.getDisplayName dow TextStyle/FULL (Locale/getDefault))
      :day_of_month         (.getDayOfMonth v)
      :day_of_week          (.get v (.dayOfWeek week-of))
      :day_of_year          (.getDayOfYear v)
      :holiday_ind          holiday?
      :is_last_day_of_month (= v (->last-day-of-month v))
      :month_abbr           (.getDisplayName moy TextStyle/SHORT (Locale/getDefault))
      :month_name           (.getDisplayName moy TextStyle/FULL (Locale/getDefault))
      :month_of_year        (.getMonthValue v)
      :month_working_days   (or (when (= (:year_month prior) year) (:month_working_days prior))
                                (working-days (->first-day-of-month v)
                                              (->last-day-of-month v)
                                              holidays))
      :quarter_abbr         (.getDisplayName qoy TextStyle/SHORT (Locale/getDefault))
      :quarter_name         (.getDisplayName qoy TextStyle/FULL (Locale/getDefault))
      :quarter_of_year      (.getValue qoy)
      :week_of_month        (.get v (.weekOfMonth week-of))
      :week_of_year         (.getWeek (YearWeek/from v))
      :weekday_ind          weekday?
      :workday_ind          (and weekday? (not holiday?))
      :working_day_of_month (working-days (->first-day-of-month v) v holidays)
      :year                 year
      :year_month           year_month
      :ytd_working_days     (or (when (= (:year prior) year) (:ytd_working_days prior))
                                (working-days (->first-day-of-year v) v holidays))})))

(defn local-dates->date-maps [date-vector opts]
  (->> date-vector sort
       (reduce
        (fn [m1 d1]
          (conj m1 (local-date->date-map d1 (assoc opts :prior (last m1)))))
        [])))

(defn- time-is-type? [time-is type-fn]
  (if (coll? time-is)
    (every? #(time-is-type? % type-fn) time-is)
    (type-fn time-is)))

(defn- time-is-compare [v time-is type-fn]
  (if (coll? time-is)
    (some
     (fn [i]
       (if (coll? i)
         (let [[start end] i]
           (apply time<= (map type-fn [start v end])))
         (apply = (map type-fn [v i]))))
     time-is)
    (apply = (map type-fn [v time-is]))))

(defn time-is?
  "Determine if input value is of certain type.

   Behavior changes based on the input:

   - If time-is is a singular weekday, month or time value:
     - v will be matched (=) to that value
   - If time-is is a collection of weekdays or months:
     - time-is will be matched (contains) to v
   - If time-is is a collection of time range collections:
     - v will be checked whether it's within any of the time ranges
   "
  [v time-is]
  (cond
    (time-is-type? time-is #(or (is-type? % DayOfWeek) (str-is-day-of-week? %)))
    (time-is-compare v time-is type->day-of-week)
    (time-is-type? time-is #(or (is-type? % Month) (str-is-month? %)))
    (time-is-compare v time-is type->month)
    (time-is-type? time-is #(or (java-time? %) (str-is-time? %)))
    (time-is-compare v time-is type->local-time)))

(defn is-now? [time-is]
  (time-is? (get-instant) time-is))