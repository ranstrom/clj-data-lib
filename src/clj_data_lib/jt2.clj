(ns clj-data-lib.jt2
  (:import (java.time Period Instant Clock LocalDateTime LocalDate LocalTime)
           (java.time.format DateTimeFormatter DateTimeFormatterBuilder ResolverStyle)
           (java.time.temporal ChronoField ChronoUnit IsoFields JulianFields)))

;; Heavily inspired/borrowed from: clojure.java-time/clojure.java-time
;; The only reason for not using clojure.java-time at this time is random compile issues.

(defonce units-iso
  {:week-based-years IsoFields/WEEK_BASED_YEARS
   :quarter-years    IsoFields/QUARTER_YEARS})

(defonce units-chrono
  {:millis     ChronoUnit/MILLIS
   :weeks      ChronoUnit/WEEKS
   :centuries  ChronoUnit/CENTURIES
   :minutes    ChronoUnit/MINUTES
   :days       ChronoUnit/DAYS
   :years      ChronoUnit/YEARS
   :seconds    ChronoUnit/SECONDS
   :nanos      ChronoUnit/NANOS
   :decades    ChronoUnit/DECADES
   :forever    ChronoUnit/FOREVER
   :hours      ChronoUnit/HOURS
   :micros     ChronoUnit/MICROS
   :millenia   ChronoUnit/MILLENNIA
   :months     ChronoUnit/MONTHS
   :half-days  ChronoUnit/HALF_DAYS
   :eras       ChronoUnit/ERAS})

(defonce fields-iso
  {:week-based-year IsoFields/WEEK_BASED_YEAR
   :quarter-of-year IsoFields/QUARTER_OF_YEAR
   :day-of-quarter  IsoFields/DAY_OF_QUARTER
   :week-of-year    IsoFields/WEEK_OF_WEEK_BASED_YEAR})

(defonce fields-chrono
  {:era               ChronoField/ERA
   :year              ChronoField/YEAR
   :year-of-era       ChronoField/YEAR_OF_ERA
   :proleptic-month   ChronoField/PROLEPTIC_MONTH
   :day-of-year       ChronoField/DAY_OF_YEAR
   :day-of-month      ChronoField/DAY_OF_MONTH
   :day-of-week       ChronoField/DAY_OF_WEEK
   :month-of-year     ChronoField/MONTH_OF_YEAR
   :hour-of-day       ChronoField/HOUR_OF_DAY
   :hour-of-ampm      ChronoField/HOUR_OF_AMPM
   :second-of-day     ChronoField/SECOND_OF_DAY
   :second-of-minute  ChronoField/SECOND_OF_MINUTE
   :milli-of-day      ChronoField/MILLI_OF_DAY
   :milli-of-second   ChronoField/MILLI_OF_SECOND
   :micro-of-day      ChronoField/MICRO_OF_DAY
   :micro-of-second   ChronoField/MICRO_OF_SECOND
   :nano-of-day       ChronoField/NANO_OF_DAY
   :nano-of-second    ChronoField/NANO_OF_SECOND
   :offset-seconds    ChronoField/OFFSET_SECONDS})

(defonce fields-julian
  {:julian-day          JulianFields/JULIAN_DAY
   :modified-julian-day JulianFields/MODIFIED_JULIAN_DAY
   :rata-die            JulianFields/RATA_DIE})

(defn- make-clock [f]
  (f (Clock/systemDefaultZone)))

(defn instant
  ([] (make-clock #(Instant/now %))))

(defn- get-case-formatter ^DateTimeFormatterBuilder [c]
  (let [fmt-builder (DateTimeFormatterBuilder.)]
    (if (= c :sensitive)
      (.parseCaseSensitive fmt-builder)
      (.parseCaseInsensitive fmt-builder))
    fmt-builder))

(defn- get-resolver-style [s]
  (if (instance? ResolverStyle s) s
      (case s
        :strict ResolverStyle/STRICT
        :smart ResolverStyle/SMART
        :lenient ResolverStyle/LENIENT)))

(defn formatter ^String
  ([fmt]
   (formatter fmt {}))
  ([fmt {:keys [resolver-style case] :or {case :sensitive}}]
   (let [^DateTimeFormatter fmt
         (cond (instance? DateTimeFormatter fmt) fmt
               (string? fmt) (.. (get-case-formatter case)
                                 (appendPattern fmt)
                                 toFormatter)
               :else (throw (Exception. (str "Unsupported formatter type: " (type fmt) " for " fmt))))
         fmt (if resolver-style
               (.withResolverStyle fmt (get-resolver-style resolver-style))
               fmt)]
     fmt)))

(defn jt-format
  "Formats the given time entity as a string."
  [fmt o]
  (.format o (formatter fmt)))

(defn local-date
  "Creates a `LocalDate`."
  ^LocalDate
  ([] (make-clock (fn [^Clock c] (LocalDate/now c))))
  ([^String date]
   (LocalDate/parse date))
  ([^String date ^String format]
   (LocalDate/parse date (DateTimeFormatter/ofPattern format)))
  ([y m d]
   (LocalDate/of (int y) (int m) (int d))))

(defn local-date-time
  "Creates a `LocalDateTime`."
  ^LocalDateTime
  ([] (make-clock (fn [^Clock c] (LocalDateTime/now c))))
  ([^String date]
   (LocalDateTime/parse date))
  ([^String date ^String format]
   (LocalDateTime/parse date (DateTimeFormatter/ofPattern format)))
  ([y m d]
   (local-date-time y m d 0))
  ([y m d h]
   (local-date-time y m d h 0))
  ([y m d h mm]
   (local-date-time y m d h mm 0))
  ([y m d h mm ss]
   (local-date-time y m d h mm ss 0))
  ([y m d h mm ss n]
   (LocalDateTime/of (int y) (int m) (int d) (int h) (int mm) (int ss) (int n))))

(defn local-time
  "Creates a `LocalTime`."
  ^LocalTime
  ([] (make-clock (fn [^Clock c] (LocalTime/now c))))
  ([h m s nn]
   (LocalTime/of (int h) (int m) (int s) (int nn))))

(defn to-sql-date
  "Converts a local date entity to a `java.sql.Date`."
  ^java.sql.Date
  [^LocalDate o] (java.sql.Date/valueOf o))

(defn to-sql-timestamp
  "Converts a date entity to a `java.sql.Timestamp`."
  ^java.sql.Timestamp
  [^LocalDateTime o]
  (java.sql.Timestamp/valueOf o))

(defn time-between [^LocalDate start-date ^LocalDate end-date frequency]
  (let [cu (get units-chrono frequency)]
    (.between cu start-date end-date)))

(defn jt-min [d1 & args]
  (reduce
   (fn [curr-min nxt]
     (if (pos? (.getDays (Period/between curr-min nxt)))
       curr-min
       nxt))
   d1
   args))

(defn jt-max [d1 & args]
  (reduce
   (fn [curr-min nxt]
     (if (pos? (.getDays (Period/between curr-min nxt)))
       nxt
       curr-min))
   d1
   args))