(ns clj-data-lib.spec.data
  (:require [clj-data-lib.data :as d]
            [clojure.spec.alpha :as s])
  (:import (clojure.lang Named)))

(s/fdef d/coalesce
  :args (s/cat :c coll?)
  :ret any?)

(s/fdef d/coll-contains?
  :args (s/cat :coll1 (s/nilable (s/coll-of keyword?))
               :v keyword?)
  :ret boolean?)

(s/fdef d/coll-of-maps?
  :args (s/cat :v any?)
  :ret boolean?)

(s/fdef d/duplicates
  :args (s/cat :c coll?)
  :ret coll?)

(s/fdef d/find-match-in-vector
  :args (s/cat :input-vector (s/coll-of map?)
               :compare-key keyword?
               :compare-value string?
               :rest (s/? (s/cat :icase? boolean?)))
  :ret (s/or :ok map? :err nil?))

(s/fdef d/map->qualified
  :args (s/cat :spec #(instance? Named %) :m map?)
  :ret map?
  :fn #(= (count (keys (:ret %)))
          (count (keys (-> % :args :m)))))

(s/fdef d/parse-number
  :args (s/cat :s string?)
  :ret (s/or :ok number? :err nil?))

(s/fdef d/str-is-number?
  :args (s/cat :a string?)
  :ret boolean?)