(ns clj-data-lib.string
  (:require [clojure.string :as string])
  (:import (java.security MessageDigest MessageDigest)))

(defn pad-left [field width & [pad]]
  (string/join (concat (take (- width (count field)) (repeat (or pad " "))) field)))

(defn pad-right [field width & [pad]]
  (string/join (take width (concat field (repeat (or pad " "))))))

(defn hex-digest
  "Returns the hex digest of an object. Expects a string as input."
  ([input]
   (hex-digest input "SHA-256"))
  ([input hash-algorithm]
   (if (string? input)
     (let [hash (MessageDigest/getInstance hash-algorithm)]
       (.update hash (.getBytes input))
       (let [digest (.digest hash)]
         (string/join
          (map #(format "%02x" (bit-and % 0xff)) digest))))
     (throw (Exception. (str "Invalid string input, got " (type input)))))))

(defn shorten
  "Use only when string max size is more important than retaining data.
   
   Perform inconsidered string shortening."
  [str1 max-size]
  (let [n (if (string? max-size) (Long/valueOf ^String max-size) max-size)]
    (if (> (count str1) n)
      (subs str1 0 n)
      str1)))

(defn shorten-utf8
  "Use only when string max size is more important than retaining data.
  
  Will substring UFT-8 based on maximum byte length."
  [input-str max-bytes]
  (if (and (string? input-str) (pos? max-bytes) (> (count input-str) max-bytes))
    (->> (vec input-str)
         (reduce
          (fn [str-data c]
            (if (or (>= (:byte-size str-data) max-bytes) (:skip-last? str-data))
              (merge str-data {:skip-last? false})
              (let [ci (int c)
                    char-byte (cond
                                (<= ci (int 0x007f)) 1
                                (<= ci (int 0x07FF)) 2
                                (<= ci (int 0xd7ff)) 3
                                (<= ci (int 0xDFFF)) 4
                                :else 3)
                    skip? (= char-byte 4)
                    new-char-pos (if skip? (inc (:char-pos str-data)) (:char-pos str-data))]
                {:byte-size  (+ (:byte-size str-data) char-byte)
                 :char-pos   (inc new-char-pos)
                 :skip-last? skip?})))
          {:byte-size 0 :char-pos 0 :skip-last? false})
         :char-pos
         (subs input-str 0))
    input-str))

(defn ci=
  "Compare two strings case insensitive. Return false if either string is nil."
  [str1 str2]
  (if (some #(or (nil? %) (not (string? %))) [str1 str2])
    false
    (= (string/lower-case str1) (string/lower-case str2))))

(defn ci-in-coll?
  "Return whether string is in vector of strings"
  [str1 coll1]
  (if (or (not (coll? coll1)) (empty? coll1))
    false
    (or (some #(ci= % str1) coll1) false)))