(ns clj-data-lib.data
  (:require [clj-data-lib.string :refer [ci=]])
  (:import (java.util UUID)))

(defn uuid [] (str (UUID/randomUUID)))

(def default-uuid "00000000-0000-0000-0000-000000000000")

(def not-nil? (complement nil?))

(defmacro or+
  "Similar to OR, but accepts false as value."
  ([] nil)
  ([x] x)
  ([x & next]
   `(let [or# ~x]
      (if (not-nil? or#) or# (or ~@next)))))

(defn coll-contains? [coll1 v]
  (cond
    (or (not (coll? coll1)) (empty? coll1)) false
    (map? coll1) (contains? coll1 v)
    :else (or (some #{v} coll1) false)))

(defn data-maps->vector
  [data-keys data]
  (map #((apply juxt data-keys) %) data))

(defn vector->data-maps
  [data-keys data]
  (map zipmap
       (->> data-keys
            (map keyword)
            repeat)
       data))

(defn coalesce [coll]
  (first (filter not-nil? coll)))

(defn duplicates
  "Find duplicates in collection."
  [c]
  (for [[id freq] (frequencies c)
        :when (> freq 1)]
    id))

(defn parse-number-leading-zeroes [s]
  (when (re-matches #"^[0]+[1-9]+\d*$" s)
    (->> s (re-seq #"[1-9]+\d*$") first)))

(defn parse-number-leading-zeroes-negative [s]
  (when (re-matches #"^-[0]+[1-9]+\d*$" s)
    (->> s (re-seq #"[1-9]+\d*$") first (str "-"))))

(defn parse-number
  "Reads a number from a string. Returns nil if not a number."
  [s]
  (when (re-find #"^-?\d+\.?\d*$" s)
    (->> s
         (#(if (re-matches #"^[0]+[1-9]+\d*$" %) (->> % (re-seq #"[1-9]+\d*$") first) %))
         (#(if (re-matches #"^-[0]+[1-9]+\d*$" %) (->> % (re-seq #"[1-9]+\d*$") first (str "-")) %))
         read-string)))

(defn str-is-number?
  "Check if string value is number."
  [a] (not-nil? (parse-number (str a))))

(defn ->qualified
  [spec-ns k]
  (if (and (keyword? k) (not (qualified-keyword? k)))
    (keyword (str spec-ns "/" (name k)))
    k))

(defn ->unqualified
  [k]
  (if (and (keyword? k) (qualified-keyword? k))
    (keyword (name k))
    k))

(defn coll-of-maps? [v]
  (if (and (coll? v) (every? map? v)) true false))

(defn- qualify-map
  ([spec-ns m]
   (qualify-map spec-ns m nil))
  ([spec-ns m exclude-fn]
   (reduce-kv
    (fn [acc k v]
      (assoc acc (->qualified spec-ns k)
             (cond
               (and (map? v) (or (nil? exclude-fn) (exclude-fn v))) (qualify-map spec-ns v exclude-fn)
               (map? v) v
               (coll-of-maps? v) (vec (map #(qualify-map spec-ns % exclude-fn) v))
               :else v)))
    {} m)))

(defn map->qualified
  "Add namespace from spec to existing map."
  ([spec m] (map->qualified spec m nil))
  ([spec m exclude-fn] (qualify-map (namespace spec) m exclude-fn)))

(defn qualified-map->unqualified
  [m]
  (reduce-kv
   (fn [acc k v]
     (assoc acc (->unqualified k)
            (cond
              (map? v) (qualified-map->unqualified v)
              (coll-of-maps? v) (vec (map qualified-map->unqualified v))
              :else v)))
   {} m))

(defn find-match-in-vector
  "Iterate through a vector of maps and return value for a particular key. Return nil if no match found.

  Optionally pass whether case insensitive compare should be done (assumes string values).

  E.g.
  Input:
  [{:table_name \"tbl1\" :table_schema \"dbo\"} {:table_name \"tbl2\" :table_schema \"dbo\"}] :table_name \"tbl1\"
  Return:
  {:table_name \"tbl1\" :table_schema \"dbo\"}
  "
  [input-vector compare-key compare-value & [ci?]]
  (some
   (fn [entry]
     (when (map? entry)
       (let [entry-value (get entry compare-key)]
         (when (or (and ci? (every? string? [entry-value compare-value])
                        (ci= entry-value compare-value))
                   (and (not ci?) (= entry-value compare-value)))
           entry))))
   input-vector))

(defn positions
  "Positions matching predicate in a collection.

   Source: https://stackoverflow.com/a/4831131
   "
  [pred coll]
  (keep-indexed
   (fn [idx x]
     (when (pred x)
       idx))
   coll))